Proposition de projet « guidé » autour de la regression logistique.
Sujet dans le fichier PDF.

### Pedestrian image classification

When the cross entropy makes the problem convex !!! ![](http://romain.raveaux.free.fr/document/pietonpaspieton.PNG)  
[Slide](https://drive.google.com/open?id=1OmGUIsDgBxSgMZl2BJdjCHlpJYXiP9n91i2oB1jllIk)  
HTML version of the notebook can be found here. [HTML Notebook](document/classificationdepietons.html)